// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <os/log.h>
#include <dispatch/dispatch.h>
#include <xpc/xpc.h>
#include <Foundation/Foundation.h>

NSMutableSet* g_connections;

@interface SKLConnection : NSObject {
}

@property(nonatomic) xpc_connection_t xpcConnection;
@property(nonatomic) BOOL subscribed;

- (id)initWithXPCConnection:(xpc_connection_t)xpcConn;
- (void)didReceiveXPCEvent:(xpc_object_t)msg;
- (void)didReceiveMessage:(xpc_object_t)msg;
- (void)sendString:(const char*)str;

@end

@implementation SKLConnection

- (id)initWithXPCConnection:(xpc_connection_t)xpcConn
{
    _xpcConnection = xpcConn;

    xpc_connection_set_event_handler(xpcConn, ^(xpc_object_t obj) {
        [self didReceiveXPCEvent:obj];
    });

    xpc_connection_resume(xpcConn);

    return self;
}
- (void)didReceiveXPCEvent:(xpc_object_t)obj
{
    if (xpc_get_type(obj) == XPC_TYPE_DICTIONARY) {
        [self didReceiveMessage: obj];
    } else if (xpc_get_type(obj) == XPC_TYPE_ERROR) {
        os_log(OS_LOG_DEFAULT, "Connection got error: %s\n", xpc_dictionary_get_string(obj, XPC_ERROR_KEY_DESCRIPTION));
        os_log(OS_LOG_DEFAULT, "removing conn %{public}@ from set\n", self);
        [g_connections removeObject: self];
    }
}
- (void)didReceiveMessage:(xpc_object_t)msg
{
    int64_t commandId = xpc_dictionary_get_int64(msg, "command");
    os_log(OS_LOG_DEFAULT, "received command with ID %lld\n", commandId);

    switch (commandId) {
        case 1:
            os_log(OS_LOG_DEFAULT, "conn %{public}@ received subscribe command\n", self);
            [self setSubscribed:YES];
            break;
        case 2:
            os_log(OS_LOG_DEFAULT, "conn %{public}@ received unsubscribe command\n", self);
            [self setSubscribed:NO];
            break;
        case 3: {
            os_log(OS_LOG_DEFAULT, "conn %{public}@ received data\n", self);
            const char* string = xpc_dictionary_get_string(msg, "data");
            if (string == NULL) {
                os_log(OS_LOG_DEFAULT, "Got command #3 with no data");
                return;
            }
            for (SKLConnection* conn in g_connections) {
                if (conn == self) continue;
                if ([conn subscribed]) {
                    [conn sendString: string];
                }
            }
        }
    }
}

- (void)sendString:(const char*)str {
    os_log(OS_LOG_DEFAULT, "sending data to conn %{public}@\n", self);
    const char* keys[] = {"command", "data"};
    xpc_object_t values[] = {xpc_int64_create(4), xpc_string_create(str)};

    xpc_object_t msg = xpc_dictionary_create(keys, values, 2);
    xpc_connection_send_message(self.xpcConnection, msg);
}

@end

void got_connection(xpc_connection_t xpcConn)
{
    xpc_connection_set_target_queue(xpcConn, dispatch_get_main_queue());
    SKLConnection* conn = [[SKLConnection alloc] initWithXPCConnection:xpcConn];
    os_log(OS_LOG_DEFAULT, "adding conn %{public}@ to set\n", conn);
    [g_connections addObject:conn];
}

int main()
{
    xpc_connection_t listenerConn = xpc_connection_create_mach_service("cy:xyz.nicolas17.sslkeylogd", dispatch_get_main_queue(), XPC_CONNECTION_MACH_SERVICE_LISTENER);

    g_connections = [[NSMutableSet alloc] init];

    xpc_connection_set_event_handler(listenerConn, ^(xpc_object_t obj) {
        if (xpc_get_type(obj) == XPC_TYPE_CONNECTION) {
            os_log(OS_LOG_DEFAULT, "Got new connection\n");
            got_connection(obj);
        } else if (xpc_get_type(obj) == XPC_TYPE_ERROR) {
            os_log(OS_LOG_DEFAULT, "Listener got an error: %s\n", xpc_dictionary_get_string(obj, XPC_ERROR_KEY_DESCRIPTION));
            exit(1);
        }
    });
    xpc_connection_resume(listenerConn);
    dispatch_main();
}
