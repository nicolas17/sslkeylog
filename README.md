# SSLKeyLog

This is a MobileSubstrate tweak that dumps TLS session keys,
so that you can decrypt TLS network traffic
captured with Wireshark or similar.
It supports iOS 12 and later
and works with both SecureTransport and BoringSSL.

I find this much more convenient than using mitmproxy,
adding the proxy's CA to the system trust store,
and trying to bypass the 100 different ways
in which apps do certificate pinning.

(There are blog posts explaining how to use Frida to do the same
[(1)](https://andydavies.me/blog/2019/12/12/capturing-and-decrypting-https-traffic-from-ios-apps/)
[(2)](https://hugotunius.se/2020/08/07/stealing-tls-sessions-keys-from-ios-apps.html),
but it only works on BoringSSL and on less iOS versions,
and well, it needs Frida.)

NOTE: This "works on my machine" but hasn't yet been tested outside my own devices.
Let me know about success/failure using it.

## Usage

To use this tweak, install the .deb package
(make sure the mobilesubstrate dependency is installed too).
Restart whatever processes
you want to capture the traffic of (or reboot)
so that the tweak is loaded.
Then run `sslkeylog` on the device,
and it will output the key log to stdout.
For example, you can get keys in real time on your computer with
```
ssh mobile@iphone sslkeylog | tee sslkeys.txt
```
Or you could run `sslkeylog > ssl.txt` directly on the device
and copy the log file later.

The key log file can then be [loaded in Wireshark](https://wiki.wireshark.org/TLS#TLS_Decryption)
to decrypt captured TLS packets.
This even works in real time;
Wireshark will read new keys from the log
when new traffic arrives.

## Future plans

* An installation script to use when dpkg is not available.
  For example if you jailbreak an unactivated device with checkra1n,
  Cydia is not yet installed
  and you need to access the home screen to install it,
  but you may want to capture the traffic of the activation itself
  before you get to the home screen.
* Currently any process (even App Store apps) could get SSL keys
  by connecting to the daemon.
  Add entitlements to prevent this.
