// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <stdio.h>
#include <stdlib.h>

#include <dispatch/dispatch.h>
#include <xpc/xpc.h>

int main(int argc, const char** argv)
{
    xpc_connection_t conn = xpc_connection_create_mach_service("cy:xyz.nicolas17.sslkeylogd", NULL, 0);

    xpc_connection_set_event_handler(conn, ^(xpc_object_t obj) {
        if (xpc_get_type(obj) == XPC_TYPE_DICTIONARY) {
            int64_t commandId = xpc_dictionary_get_int64(obj, "command");
            if (commandId == 4) {
                const char* data = xpc_dictionary_get_string(obj, "data");
                printf("%s\n", data);
                fflush(stdout);
            } else {
                fprintf(stderr, "Got unknown XPC message #%lld\n", commandId);
            }
        } else if (xpc_get_type(obj) == XPC_TYPE_ERROR) {
            fprintf(stderr, "Got an error: %s\n", xpc_dictionary_get_string(obj, XPC_ERROR_KEY_DESCRIPTION));
            exit(1);
        }
    });

    xpc_connection_resume(conn);

    dispatch_async(dispatch_get_main_queue(), ^() {
        if (argc > 1) {
            const char* keys[] = {"command", "data"};
            xpc_object_t values[] = {xpc_int64_create(3), xpc_string_create(argv[1])};
            xpc_object_t msg = xpc_dictionary_create(keys, values, 2);
            xpc_connection_send_message(conn, msg);
            fprintf(stderr, "sent data\n");
            exit(0);
        } else {
            const char* key = "command";
            xpc_object_t num = xpc_int64_create(1);
            xpc_object_t msg = xpc_dictionary_create(&key, &num, 1);
            xpc_connection_send_message(conn, msg);
            fprintf(stderr, "subscribed\n");
        }
    });

    dispatch_main();
}
