// SPDX-FileCopyrightText: 2021 Nicolás Alvarez <nicolas.alvarez@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <CydiaSubstrate/CydiaSubstrate.h>
#include <CoreFoundation/CoreFoundation.h>
#include <Security/SecureTransport.h>

#include <os/log.h>
#include <xpc/xpc.h>

void sendLineToDaemon(const char* line) {
    xpc_connection_t conn = xpc_connection_create_mach_service("cy:xyz.nicolas17.sslkeylogd", NULL, 0);
    xpc_connection_set_event_handler(conn, ^(xpc_object_t obj) {});
    xpc_connection_resume(conn);

    const char* keys[] = {"command", "data"};
    xpc_object_t values[] = {xpc_int64_create(3), xpc_string_create(line)};
    xpc_object_t msg = xpc_dictionary_create(keys, values, 2);
    xpc_connection_send_message(conn, msg);
    xpc_connection_cancel(conn);
}

// SecureTransport {{{

/* The size of of client- and server-generated random numbers in hello messages. */
#define SSL_CLIENT_SRVR_RAND_SIZE		32

/* The size of the pre-master and master secrets. */
#define SSL_RSA_PREMASTER_SECRET_SIZE	48
#define SSL_MASTER_SECRET_SIZE			48

/*
 * For the following three functions, *size is the available
 * buffer size on entry and the actual size of the data returned
 * on return. The above consts are for convenience.
 */
OSStatus SSLInternalMasterSecret(
   SSLContextRef context,
   void *secret,         // mallocd by caller, SSL_MASTER_SECRET_SIZE
   size_t *secretSize);  // in/out

OSStatus SSLInternalServerRandom(
   SSLContextRef context,
   void *randBuf, 			// mallocd by caller, SSL_CLIENT_SRVR_RAND_SIZE
   size_t *randSize);	// in/out

OSStatus SSLInternalClientRandom(
   SSLContextRef context,
   void *randBuf,  		// mallocd by caller, SSL_CLIENT_SRVR_RAND_SIZE
   size_t *randSize);	// in/out


OSStatus (*origSSLHandshake)(SSLContextRef);

static void tohex(const uint8_t* input, size_t len, char* output) {
    for (size_t i=0; i<len; i++) {
		output[i*2]   = "0123456789ABCDEF"[input[i] >> 4];
		output[i*2+1] = "0123456789ABCDEF"[input[i] & 0x0F];
	}
    output[len*2]=0;
}

static OSStatus mySSLHandshake(SSLContextRef context) {
    os_log(OS_LOG_DEFAULT, "Tweak: ST handshaking");
    OSStatus status = origSSLHandshake(context);
    if (status == 0) {
        size_t random_size=SSL_CLIENT_SRVR_RAND_SIZE;
        size_t secret_size=SSL_MASTER_SECRET_SIZE;
        uint8_t random[SSL_CLIENT_SRVR_RAND_SIZE];
        uint8_t secret[SSL_MASTER_SECRET_SIZE];

        OSStatus result_r = SSLInternalClientRandom(context, random, &random_size);
        OSStatus result_s = SSLInternalMasterSecret(context, secret, &secret_size);

        char hexrandom[SSL_CLIENT_SRVR_RAND_SIZE*2+1]={0};
        char hexsecret[SSL_MASTER_SECRET_SIZE*2+1]={0};
        tohex(random, random_size, hexrandom);
        tohex(secret, secret_size, hexsecret);

        os_log(OS_LOG_DEFAULT, "Tweak: ST requesting random returned status %d and size %lu", (int)result_r, random_size);
        os_log(OS_LOG_DEFAULT, "Tweak: ST requesting secret returned status %d and size %lu", (int)result_s, secret_size);
        os_log(OS_LOG_DEFAULT, "Tweak: ST: CLIENT_RANDOM %{public}s %{public}s", hexrandom, hexsecret);

        // 13 for "CLIENT_RANDOM" + 2 for spaces + 1 for null = 16, but add a few more bytes to be safe
        char line[SSL_CLIENT_SRVR_RAND_SIZE*2 + SSL_MASTER_SECRET_SIZE*2 + 20];

        int bytes_needed = snprintf(line, sizeof(line), "CLIENT_RANDOM %s %s", hexrandom, hexsecret);
        if (bytes_needed >= sizeof(line)) {
            os_log(OS_LOG_DEFAULT, "Tweak: snprintf somehow needed more chars than expected?? %d >= %lu", bytes_needed, sizeof(line));
        } else {
            sendLineToDaemon(line);
        }
    } else {
        os_log(OS_LOG_DEFAULT, "Tweak: ST handshake returned %d", (int)status);
    }

    return status;
}
// }}}

// BoringSSL {{{

#define SSL_MAX_MASTER_KEY_LENGTH 48

typedef struct _SSL SSL;
typedef struct _SSL_SESSION SSL_SESSION;
typedef struct _SSL_CTX SSL_CTX;
typedef struct _SSL_METHOD SSL_METHOD;

SSL_CTX *SSL_CTX_new(const SSL_METHOD *method);
void SSL_CTX_set_keylog_callback(SSL_CTX *ctx, void (*cb)(const SSL *ssl, const char *line));

SSL_CTX *(*orig_SSL_CTX_new)(const SSL_METHOD *method);

// offsets of the keylog callback, retrieved by disassembling libboringssl
// from the dyld_shared_cache, looking at function bssl::ssl_log_secret:
//
// 2b0 iPhone_4.0_64bit_12.0_16A366_Restore.ipsw
// 2b0 iPhone_4.0_64bit_12.0.1_16A404_Restore.ipsw
// 2a8 iPhone_4.0_64bit_12.1_16B5059d_Restore.ipsw
// 2a8 iPhone_4.0_64bit_12.1_16B92_Restore.ipsw
// 2a8 iPhone_4.0_64bit_12.2_16E227_Restore.ipsw
// 2a8 iPhone_4.0_64bit_12.5_16H20_Restore.ipsw
// 2b0 iPhone_4.7_13.0_17A577_Restore.ipsw
// 2b0 iPhone_4.7_13.1_17A844_Restore.ipsw
// 2b0 iPhone_4.7_13.2_17B84_Restore.ipsw
// 2b0 iPhone_4.7_13.2.3_17B111_Restore.ipsw
// 2c0 iPhone_4.7_13.3_17C54_Restore.ipsw
// 2c0 iPhone_4.7_13.5_17F75_Restore.ipsw
// 2c0 iPhone_4.7_13.7_17H35_Restore.ipsw
// 2b8 iPhone_4.7_14.0_18A373_Restore.ipsw
// 2b8 iPhone_4.7_14.3_18C66_Restore.ipsw

// so we end up with:
// [12.0, 12.1) 2b0
// [12.1, 13.0) 2a8
// [13.0, 13.3) 2b0
// [13.3, 14.0) 2c0
// [14.0, 15.0) 2b8

struct OffsetItem {
    double cfVersion;
    size_t fieldOffset;
};

static const struct OffsetItem offsetTable[] = {
    {1556.00,  0x2b0}, // 12.0
    {1560.10,  0x2a8}, // 12.1
    {1665.15,  0x2b0}, // 13.0
    {1674.102, 0x2c0}, // 13.3
    {1751.108, 0x2b8}, // 14.0
    {1770.300, -1}     // 14.3
};

static size_t getFieldOffset(double cfVersion) {
    for (int i=0; i<sizeof(offsetTable)/sizeof(struct OffsetItem)-1; ++i) {
        if (cfVersion >= offsetTable[i].cfVersion &&
            cfVersion <  offsetTable[i+1].cfVersion
        ) {
            os_log(OS_LOG_DEFAULT, "Tweak: Matched CF version %.3f between %.3f and %.3f",
                cfVersion,
                offsetTable[i].cfVersion,
                offsetTable[i+1].cfVersion
            );
            os_log(OS_LOG_DEFAULT, "Tweak: Will use struct offset %lx", offsetTable[i].fieldOffset);
            return offsetTable[i].fieldOffset;
        }
    }
    return -1;
}

static size_t keylogCallbackOffset = -1;

static void boringssl_keylog(const SSL *ssl, const char *line) {
    os_log(OS_LOG_DEFAULT, "Tweak: BSSL: %{public}s", line);
    sendLineToDaemon(line);
}

static SSL_CTX *my_SSL_CTX_new(const SSL_METHOD *method) {
    SSL_CTX* ctx = orig_SSL_CTX_new(method);
    os_log(OS_LOG_DEFAULT, "Tweak: BSSL creating context");

    void** keylog_callback = (void**)((char*)ctx + keylogCallbackOffset);
    *keylog_callback = &boringssl_keylog;

    return ctx;
}

// }}}


MSInitialize {
    os_log(OS_LOG_DEFAULT, "Tweak: loading3");
    // SecureTransport
    MSHookFunction(&SSLHandshake, &mySSLHandshake, (void*)&origSSLHandshake);
    // BoringSSL
    keylogCallbackOffset = getFieldOffset(kCFCoreFoundationVersionNumber);
    if (keylogCallbackOffset > 0) {
        MSHookFunction(&SSL_CTX_new, &my_SSL_CTX_new, (void*)&orig_SSL_CTX_new);
    } else {
        os_log(OS_LOG_DEFAULT, "Tweak: iOS version not supported; BoringSSL offset in struct not known");
    }
}
